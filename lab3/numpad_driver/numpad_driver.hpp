#include <vector>
#include <string>
#include <iostream>
#include "GPIO.hpp"


class NumpadDriver
{
public:
	NumpadDriver();
	void init();
	int check();


private:
	std::vector<GPIO> 	_columns;
	std::vector<GPIO> 	_rows;
	char _buttons[4][4];

};
