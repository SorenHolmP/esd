#include <iostream>
#include "numpad_driver.hpp"

#include "GPIO.hpp"

using std::cout;
using std::endl;


int main()
{
	NumpadDriver npd;
	npd.init();
	npd.check();
	
	GPIO led_pin("984");			// calls the empty constructor

	led_pin.exportPin();
	led_pin.setPinDirection("out");
	led_pin.setPinValue("1");
	
	cout << "hello welt" << endl;
	
	return 0;
}
