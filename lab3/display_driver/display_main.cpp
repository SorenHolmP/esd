#include <iostream>
#include <unistd.h>
#include "GPIO.hpp"
#include "display_driver.hpp"
using std::cout;
using std::endl;


int main()
{
	DisplayDriver dd;
	dd.init();
	dd.sendData("00110000");

	dd.sendData('2');
	dd.sendCommand(CMD_RETURN_HOME);

	usleep(5000 * 1000);

	dd.clear();

	dd.print(0,"Soren");
	dd.print(2, "Holm-Petersen");

	usleep(5000 * 1000);


	return 0;
	
}
