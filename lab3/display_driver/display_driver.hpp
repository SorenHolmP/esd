#include <iostream>
#include <vector>
#include <string>
#include <unistd.h> //usleep(µs)
#include <bitset>
#include <assert.h>
#include "GPIO.hpp"

const std::string CMD_CLEAR_DISPLAY  = "00000001";
const std::string CMD_RETURN_HOME    = "00000010";

class DisplayDriver
{
    public:
    DisplayDriver();

    void init();
    void print(int line_number, std::string message);
    void clear();

    //private:
    void pulseEnableSignal();
    void setDataBits(std::string byte);

    void sendData(std::string byte);
    void sendData(char character);
    void sendCommand(std::string byte); 

    private:
    std::vector<GPIO> _data_pins;
    GPIO _rs_pin;
    GPIO _rw_pin;
    GPIO _e_pin;
    GPIO _nc_pin;

};