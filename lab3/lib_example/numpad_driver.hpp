#include <vector>
#include <string>
#include <iostream>
#include "GPIO.hpp"



#define COL1 1
#define COL2 2
#define COL3 3
#define COL4 4

#define ROW1 1
#define ROW2 2
#define ROW3 3
#define ROW4 4


class NumpadDriver
{
public:
	NumpadDriver();
	void init();
	int check();


private:
	std::vector<GPIO> _columns;
	std::vector<GPIO> _rows;

};
