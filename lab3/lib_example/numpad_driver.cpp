#include "numpad_driver.hpp"

using namespace std;


NumpadDriver::NumpadDriver()
{
	
}

void NumpadDriver::init()
{
	_columns = {GPIO("995"), GPIO("994"), GPIO("993"), GPIO("992")}; //Decrementing because "995" -> COL1
	_rows = {GPIO("999"), GPIO("998"), GPIO("997"), GPIO("996")};    //"999" -> ROW1


	//Set up the column pins
	for(auto column : _columns)
	{
		column.exportPin();
		column.setPinDirection("out");
	}

	//Set up the row pins
	for(auto row : _rows)
	{
		row.exportPin();
		row.setPinDirection("in");
	}


}


int NumpadDriver::check()
{
	//Only supports one button push at a time for now
	int column_pushed = 0;
	int row_pushed	= 0;
	

	//Set all columns high to begin with:
	for(auto column : _columns)
	{
		column.setPinValue("1");
	}

	bool row_found = false;
	for(int i = 0; i < _columns.size() && !row_found; i++)
	{
		_columns[i].setPinValue("0");
		for(int j = 0; j < _rows.size(); j++)
		{
			string row_value = "-1";
			_rows[j].getPinValue(row_value);
			if(row_value == "0")
			{
				column_pushed = i + 1;
				row_pushed = j + 1;
				row_found = true;
				break;
			}
		}

	}

	if(column_pushed == 0 || row_pushed == 0)
	{
		cout << "You did not press a button" << endl;
		return -1; //No button press
	}

	
	//This takes care of buttons 1-9:
	if(column_pushed <= 3 && row_pushed <= 3) 
	{
		int button_pushed = (row_pushed - 1) * 3 + column_pushed;
		cout << "You pushed button: " <<  button_pushed << endl;
	}

	//Write code for A,B,C,D,0,F,E
	


	cout << "column pushed: " << column_pushed << endl;
	cout << "row pushed: " << row_pushed << endl;



	//if no button pushed return -1
	return -1;


}
