/*
 * GPIO.h
 *
 *  Created on: Oct 8, 2019
 *      Author: soren
 */

#ifndef GPIO_HPP_
#define GPIO_HPP_

#include <string>
#include <fstream>
#include <assert.h>


class GPIO {
public:
	GPIO();
	GPIO(std::string pin_number);
	void setPinNumber(int pinNumber);
	void exportPin();
	void unexportPin();
	void setPinDirection(std::string direction);
	void setPinValue(std::string value);
	void getPinValue(std::string &value);

	std::string getPinDirection();

private:
	std::string _pin_number;
	std::string _direction;
};

#endif /* GPIO_HPP_ */
