/*
* GPIO.cpp
*
*  Created on: Oct 8, 2019
*      Author: soren
*/

#include "GPIO.hpp"

#include <iostream>
using namespace std;

GPIO::GPIO() {
	// TODO Auto-generated constructor stub
}

GPIO::GPIO(string pin_number)
{
	_pin_number = pin_number;
}

void GPIO::setPinNumber(int pin_number)
{
	_pin_number = to_string(pin_number);
}

void GPIO::exportPin()
{

	ofstream export_file("/sys/class/gpio/export");

	//ifstream gpio_file("/sys/class/gpio/gpio" + _pin_number);
	if(export_file.is_open())
	{
//		cout << "opened export file " << _pin_number << endl;
		export_file << _pin_number;
	}
	else
	{
		cout << "Failed to open export file" << endl;
	}

}

void GPIO::unexportPin()
{
	ofstream unexport_file("/sys/class/gpio/unexport");
	if(unexport_file.is_open())
	{
//		cout << "opened export file " << _pin_number << endl;
		unexport_file << _pin_number;
	}
	else
	{
		cout << "Failed to open unexport file" << endl;
	}
}

void GPIO::setPinDirection(string direction)
{
	//assert(direction == "in" || direction == "out"); //Direction input was neither "in" nor "out"

	_direction = direction;
	ofstream direction_file("/sys/class/gpio/gpio" + _pin_number + "/direction");
	if(direction_file.is_open())
	{
		cout << "Opened direction file" << endl;
		direction_file << direction;
	}
	else
	{
		cout << "Failed to open direction file" << endl;
	}
}

void GPIO::setPinValue(string value)
{
	//assert(_direction == "out"); //Should only write to a pin which is output
	ofstream value_file("/sys/class/gpio/gpio" + _pin_number + "/value");
	if(value_file.is_open())
	{
//		cout << "Opened value file" << endl;
		value_file << value;
	}
	else
	{
		cout << "Failed to open value file" << endl;
	}
}


void GPIO::getPinValue(string &value)
{
	//assert(_direction == "in"); //Should only read from a pin which is input

	ifstream value_file("/sys/class/gpio/gpio" + _pin_number + "/value");
	if(value_file.is_open())
	{
		cout << "Opened value file" << endl;
		value_file >> value;
	}
	else
	{
		cout << "Failed to open value file" << endl;
	}

}

std::string GPIO::getPinDirection()
{
	return _direction;
}