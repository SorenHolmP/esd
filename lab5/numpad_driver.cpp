#include "numpad_driver.hpp"

using namespace std;


NumpadDriver::NumpadDriver()
{
	_buttons[0][0] = '1'; _buttons[0][1] = '2'; _buttons[0][2] = '3'; _buttons[0][3] = 'A';
	_buttons[1][0] = '4'; _buttons[1][1] = '5'; _buttons[1][2] = '6'; _buttons[1][3] = 'B';
	_buttons[2][0] = '7'; _buttons[2][1] = '8'; _buttons[2][2] = '9'; _buttons[2][3] = 'C';
	_buttons[3][0] = '0'; _buttons[3][1] = 'F'; _buttons[3][2] = 'E'; _buttons[3][3] = 'D';
}

void NumpadDriver::init()
{
	_columns = {GPIO("995"), GPIO("994"), GPIO("993"), GPIO("992")}; //Decrementing because "995" -> COL1
	_rows = {GPIO("999"), GPIO("998"), GPIO("997"), GPIO("996")};    //"999" -> ROW1


	//Set up the column pins
	for(auto& column : _columns)
	{
		column.exportPin();
		column.setPinDirection("out");
	}

	//Set up the row pins
	for(auto& row : _rows)
	{
		row.exportPin();
		row.setPinDirection("in");
	}


}


int NumpadDriver::check()
{
	//Only supports one button push at a time for now
	int column_pushed = -1;
	int row_pushed	= -1;
	

	//Set all columns high to begin with:
	for(auto& column : _columns)
	{
		column.setPinValue("1");
	}

	bool row_found = false;
	for(int i = 0; i < _columns.size() && !row_found; i++)
	{
		_columns[i].setPinValue("0");
		for(int j = 0; j < _rows.size(); j++)
		{
			string row_value = "-1";
			_rows[j].getPinValue(row_value);
			if(row_value == "0")
			{
				column_pushed = i;
				row_pushed = j;
				row_found = true;
				break;
			}
		}

	}

	if(column_pushed == -1 || row_pushed == -1)
	{
		cout << "You did not press a button" << endl;
		return -1; //No button press
	}



	//This takes care of buttons 1-9:
	// if(column_pushed <= 3 && row_pushed <= 3)
	// {
	// 	int button_pushed = (row_pushed - 1) * 3 + column_pushed;
	// 	cout << "You pushed button: " <<  button_pushed << endl;
	// }

	//Write code for A,B,C,D,0,F,E
	

	cout << "column pushed: " << column_pushed << endl;
	cout << "row pushed: " << row_pushed << endl;


	char button_pushed = _buttons[row_pushed][column_pushed];
	cout << "You pushed button: " << button_pushed << endl;

	return (int) button_pushed;


}
