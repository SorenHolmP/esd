/*
* GPIO.cpp
*
*  Created on: Oct 8, 2019
*      Author: soren
*/

#include "GPIO.hpp"

#include <iostream>
using namespace std;

GPIO::GPIO() {
	// TODO Auto-generated constructor stub
}

GPIO::GPIO(string pin_number)
{
	_pin_number = pin_number;
}

void GPIO::setPinNumber(int pin_number)
{
	_pin_number = to_string(pin_number);
}

void GPIO::writeFile(string filepath, string value)
{
	ofstream file(filepath);
	if(file.is_open())
	{
		file << value;
	}
	else
		cout << "Could not open file: " << filepath << endl;
}

void GPIO::readFile(string filepath, string &value)
{
	ifstream file(filepath);
	if(file.is_open())
	{
		file >> value;
	}
	else
		cout << "Could not open file: " << filepath << endl;
}



void GPIO::exportPin()
{
	string filepath = "/sys/class/gpio/export";
	writeFile(filepath, _pin_number);
}

void GPIO::unexportPin()
{
	string filepath = "/sys/class/gpio/unexport";
	writeFile(filepath, _pin_number);
}

void GPIO::setPinDirection(string direction)
{
	string filepath = "/sys/class/gpio/gpio" + _pin_number + "/direction";
	writeFile(filepath, direction);
}

void GPIO::setPinValue(string value)
{
	string filepath = "/sys/class/gpio/gpio" + _pin_number + "/value";
	writeFile(filepath, value);
}


void GPIO::getPinValue(string &value)
{
	string filepath = "/sys/class/gpio/gpio" + _pin_number + "/value";
	readFile(filepath, value);
}
