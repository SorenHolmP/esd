//C++
#include <iostream>
#include <string>
//C
#include <pthread.h>
#include <mqueue.h>
#include <string.h> //strerror
#include <unistd.h> //usleep

#include "GPIO.hpp"
#include "numpad_driver.hpp"
#include "display_driver.hpp"
using namespace std;

void* numpad_thread_func(void*);
void* display_thread_func(void*);


#define QUEUE_NAME  "/message_queue"
#define MAX_MSG     10
#define MSG_SIZE    100
#define EXIT_STRING "EXIT"


int main()
{
	GPIO led_pin("984");			// calls the empty constructor

	led_pin.exportPin();
	led_pin.setPinDirection("out");
	led_pin.setPinValue("1");

	// NumpadDriver npd;
	// npd.init();
	// npd.check();

	// DisplayDriver dpd;
	// dpd.init();
	// dpd.print(2,"soren");


	if(mq_unlink(QUEUE_NAME) == 0)
	        cout << "Unlinked " << QUEUE_NAME << endl;

	struct mq_attr attr;
	
	attr.mq_maxmsg  = MAX_MSG;
	attr.mq_msgsize = MSG_SIZE;
	mode_t mode = 0666; //SIX SIX SIX THE NUMBER OF THE BEAST


	mqd_t queue_handle = mq_open(QUEUE_NAME, O_CREAT | O_WRONLY, mode, &attr);
	if(queue_handle == (mqd_t) -1)
		cout << "Could not open queue: " << strerror(errno) << endl;


	pthread_t numpad_thread;
	pthread_t display_thread;
	pthread_create(&numpad_thread, NULL, numpad_thread_func, NULL);
	pthread_create(&display_thread, NULL, display_thread_func, NULL);

	pthread_join(numpad_thread, NULL);
	pthread_join(display_thread, NULL);

    return 0;

}

void* numpad_thread_func(void*)
{

	NumpadDriver npd;
	npd.init();

	mqd_t queue_handle = mq_open(QUEUE_NAME, O_WRONLY);
	if(queue_handle == (mqd_t) - 1)
		cout << "Could not open queue: " << strerror(errno) << endl;

	string key_pressed, key_pressed_old;
	while(true)
	{
		sleep(0.01);
		key_pressed = to_string( npd.check() );
		if(key_pressed != "-1" && key_pressed != key_pressed_old)
		{
			if(mq_send(queue_handle, key_pressed.c_str(), MSG_SIZE, NULL) == -1)
				cout << "Could not send key press to queue: " << strerror(errno) << endl;
			key_pressed_old = key_pressed;
		}
	}
}



void* display_thread_func(void*)
{
	DisplayDriver dd;
	dd.init();

	mqd_t queue_handle = mq_open(QUEUE_NAME, O_RDONLY);
	if(queue_handle == (mqd_t) -1)
		cout << "Could not open queue: " << strerror(errno) << endl;

	char msg[100];
	string msgstr;
	while(true)
	{
		sleep(0.01);
		if( mq_receive(queue_handle, msg, MSG_SIZE, NULL) == -1 )
			cout << "Could not receive message: " << strerror(errno) << endl;
		else
		{
			cout << "Received: " << msg << endl;
			msgstr = msg;
			dd.print(1,msgstr);
		}
	}

	cout << "Hello from display thread" << endl;
}

