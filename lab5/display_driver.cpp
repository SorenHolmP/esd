#include "display_driver.hpp"

using namespace std;

DisplayDriver::DisplayDriver()
{
    cout << "Just created a display driver" << endl;
}

void DisplayDriver::init()
{
    _data_pins = {GPIO("1023"),GPIO("1022"),GPIO("1021"),GPIO("1020"),
     GPIO("1019"),GPIO("1018"),GPIO("1017"),GPIO("1016")}; //[DB7,...,DB0]

    _rs_pin = GPIO("1012");
    _rw_pin = GPIO("1013");
    _e_pin  = GPIO("1014");
    _nc_pin = GPIO("1015");

    for(GPIO& data_pin : _data_pins)
        data_pin.exportPin();

    _rs_pin.exportPin();    
    _rw_pin.exportPin();
    _e_pin.exportPin();
    _nc_pin.exportPin();   


    for(GPIO& data_pin : _data_pins)
        data_pin.setPinDirection("out");

    _rs_pin.setPinDirection("out");    
    _rw_pin.setPinDirection("out");
    _e_pin.setPinDirection("out");
    _nc_pin.setPinDirection("out");


    //Start up sequence:
    int time_scale = 5; //Delay everything time_scale times
    usleep(20 * 1000 * time_scale);
    setDataBits("00111000"); //Function Set
    pulseEnableSignal();


    usleep(37 * time_scale);
    setDataBits("00001111"); //Display On/Off Control
    pulseEnableSignal();

    usleep(37 * time_scale);
    setDataBits("00000001");
    pulseEnableSignal();

    usleep(1.520 * 1000 * time_scale);


}

void DisplayDriver::print(int line_number, std::string message)
{
    sendCommand(CMD_CLEAR_DISPLAY); //<--This effectively means you
                                    // cannot print on two lines at the same time 
                                    // Because second invoke clears display 

    if(line_number == 2) //Only switch to line two if user of this function is capable of putting int = 2 into function
        sendCommand("11000000"); //set DDRAM address to 0x40

    for(char character : message)
        sendData(character);
}


void DisplayDriver::clear()
{
    _rs_pin.setPinValue("0");
    _rw_pin.setPinValue("0");
    setDataBits("00000001");
    pulseEnableSignal();
}

void DisplayDriver::pulseEnableSignal()
{
    _e_pin.setPinValue("1");
    usleep(1000); //Sleep 1ms
    _e_pin.setPinValue("0");
}

void DisplayDriver::setDataBits(string byte)
{
    assert(byte.length() == 8); 

    for(int i = 0; i < byte.length(); i++)
    {
        char bit = byte[i];
        string bit_string = {bit}; //conversion char -> string
        _data_pins[i].setPinValue(bit_string);
    }        
}


//Sends data to DDRAM
void DisplayDriver::sendData(string byte)
{
    _rs_pin.setPinValue("1");
    _rw_pin.setPinValue("0");
    setDataBits(byte);
    pulseEnableSignal();
    _rs_pin.setPinValue("0");

}

void DisplayDriver::sendData(char character)
{
    bitset<8> bitset_character(character);
    sendData( bitset_character.to_string() );
}


void DisplayDriver::sendCommand(std::string byte)
{
    _rs_pin.setPinValue("0");
    _rw_pin.setPinValue("0"); //Cannot use this function to read busy flag / or read data toRAM


    setDataBits(byte);
    pulseEnableSignal();

}
